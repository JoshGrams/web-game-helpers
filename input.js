// SPDX-FileCopyrightText: 2024 Joshua I. Grams <himself@joshgrams.com>
//
// SPDX-License-Identifier: MIT

// Basically you call Input() to install the event listeners and
// return an input object. You can pass a callback function
// `changed(inputName, value, change)` or you can set it on the
// input object later. You can also pass an `element` to hook the
// keyboard event listeners to, if you don't want them attached to
// the document itself.
//
// But I usually just call it with no arguments: I poll the
// current input states in my game's update function, so I don't
// need a callback; and I'm generally writing pages where the
// whole thing is a game, not ones where I need the game to be
// contained in a smaller element that isn't running all of the
// time.
//
// Then I call input.update() from my game's update function.
//
// And finally, I poll for input with input.get:
//
//    input.get({action1: "input1", action2: ["input2", "input3"],
//    ...})
//
// For keys you can pass scancode values (position of the key on a
// QWERTY keyboard) like "kb.J" or "kb.Space" or "kb.ArrowDown".
// You can also pass what I'm calling "character" values like
// "ch.J" etc., but that's usually a bad idea for games because
// they can move around on different keyboard layouts.
//
// For gamepad inputs you use "gp1.dpadLeft", "gp3.lStickX", and
// so on. If you leave out the index number ("gp.") that means
// "all gamepads" so if a player has several plugged in it will
// catch whichever one they pick up and use.
//
// You may want to negate an input (to flip an axis or combine
// two buttons into an axis): use e.g. "-kb.ArrowLeft" for this.
//
// You might also want to apply a threshold to an axis to use it
// as a button: thresholds are specified in percent (0 to 100)
// like "gp.lTrigger/20" (after the input name with a slash.
//
// I don't have any general-purpose joysticks so I'm not sure if
// they show up on the gamepad API, but you can also use "gp1.b5"
// or "gp1.axis0" for the buttons and axes on a non-standard
// "gamepad" so I suspect they will show up? Joysticks and
// gamepads do show up in the same interface in most game input
// APIs that I've used: the only difference is that the libraries
// have predefined mappings for "standard" gamepads so you can
// refer to the elements by location even though the underlying
// numberings vary quite a bit.


// installs event listeners and returns an object with properties
// for callbacks and tables, etc. (see comments below).
const Input = (changed, element) => {
	changed ??= (name, value, change) => {}
	const state = {}
	element ??= document.documentElement

	const nameRE = /^(-)?([^\/]+)(\/[0-9]+)?$/
	const _get = (state, name) => {
		const m = nameRE.exec(name)
		const sign = m[1] == null ? 1 : -1
		name = m[2]
		let value = sign * (state[name]??0)
		if(m[3] != null) {
			value = value > (+m[3].substr(1))/100 ? 1 : 0
		}
		return value
	}

	const Inp = {
		changed: changed,  // callback(name, value, changed, {key:key})
		state: state,  // current state of input buttons/keys/axes
		// Get an object with input values from a configuration object:
		//    { left: ['kb.W', 'kb.ArrowLeft', 'gp1.dpadLeft'], jump: 'kb.space', ... }
		get: (inputs, values) => {
			values ??= {}
			for(const name in inputs) if(Object.hasOwn(inputs, name)) {
				const input = inputs[name]
				if(Array.isArray(input)) {
					values[name] = input.reduce((a,nm)=>merge(a,_get(state,nm)),0)
				} else {
					values[name] = _get(state,input)
				}
			}
			return values
		},
	}

	// ------------------------------------------------------------------
	// Convenience functions

	// Merge inputs: add the largest positive and largest negative value.
	const merge = (...inp) => Math.max(0,...inp) + Math.min(0,...inp)
	Inp.merge = merge

	const clamp = (x, lo, hi) => Math.max(lo, Math.min(x, hi))

	// Dead zone for a two-axis stick.
	const dead = (x, y, fraction) => {
		const d2 = x*x + y*y
		if(d2 > 0.001) {
			const d = Math.sqrt(d2)
			const k = clamp((d-fraction)/(1-fraction), 0, 1) / d
			x *= k;  y *= k
		}
		return [x, y]
	}
	Inp.dead = dead

	const smooth = (xSmoothed, ySmoothed, x, y, dt, smoothTime) => {
		const k = 1 - Math.pow(0.05, dt/smoothTime)
		xSmoothed += k*(x-xSmoothed)
		ySmoothed += k*(y-ySmoothed)
		return [xSmoothed, ySmoothed]
	}
	Inp.smooth = smooth

	// ------------------------------------------------------------------
	// Keyboard handling

	// I feel like these aren't used much in games, and I use
	// F12 all the time to toggle the browser's debug console.
	Inp.ignoreKeys = new Set('F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12'.split(' '))

	const keyPrefix = /^Key|^Digit/
	const keyEvent = (e, press) => {
		if(e.isComposing || Inp.ignoreKeys.has(e.code)) return
		// if modifiers are down, don't block default behavior,
		// to avoid blocking browser keyboard shortcuts
		const modified = e.shiftKey||e.altKey||e.ctrlKey||e.metaKey
		if(!modified) e.preventDefault()
		// Convert to single-character codes for letter and number keys
		const code = e.code.replace(keyPrefix, '')
		// XXX - doesn't do anything about numbers vs. punctuation.
		// But that varies per keyboard layout so probably we should
		// leave it alone...? Just use scancodes for your game, OK?
		const key = e.key === ' ' ? 'Space' : (e.key.length===1 ? e.key.toUpperCase() : e.key)
		Inp.changed('kb.'+code, press, press-(Inp.state['kb.'+code]??0), {key: key})
		state['kb.'+code] = press  // kb for KeyBoard (positional, usually)
		state['ch.'+key] = press   // ch for CHaracter sent (kinda)
	}
	const keydown = e => keyEvent(e, 1)
	const keyup = e => keyEvent(e, 0)
	element.addEventListener('keydown', keydown)
	element.addEventListener('keyup', keyup)
	// Clear held keys when we lose focus, so they don't get "stuck"
	// if we miss the keyup events.
	const blur = () => {
		for(const key in Inp.state) if(Object.hasOwn(Inp.state, key)) {
			delete Inp.state[key]
		}
	}
	window.addEventListener('blur', blur)
	Inp.uninstall = () => {
		element.removeEventListener('keydown', keydown)
		element.removeEventListener('keydown', keyup)
		element.removeEventListener('blur', blur)
	}

	// We default to "scancodes" (event.code) which are named by
	// position, so W is the key where W would be on a QWERTY
	// keyboard. This is usually what you want for games: your
	// movement keys should stay in a cluster even if the letters
	// WASD are spread all over the keyboard because the player is
	// using a dvorak or azerty layout or whatever.
	//
	// For code values, see
	// https://developer.mozilla.org/en-US/docs/Web/API/UI_Events/Keyboard_event_code_values
	// except that we remove the "Key" and "Digit" prefixes so you
	// can say X or 8 instead of KeyX and Digit8.
	Inp.getKey = function(...codes) {
		return codes.some(s => Array.isArray(s) ?
			this.getKey(...s) : !!this.state['kb.'+s])
	}

	// But we track the "event.key" positions as well: sometimes you
	// do care about the character that the key produces and
	// not the position of the key.
	Inp.getChar = function(...keys) {
		return keys.some(k => Array.isArray(k) ?
			this.getChar(...k) : !!this.state['ch.'+k])
	}

	// This function makes it easy to get boolean input values
	// from a configuration object mapping keys to named actions:
	//    { left: ['W', 'ArrowLeft'], jump: ' ', ... }
	Inp.getKeys = function(codes, values) {
		values ??= {}
		for(const name in codes) {
			values[name] = Inp.getKey(codes[name])
		}
		return values
	}

	// ------------------------------------------------------------------
	// Gamepad handling: requires polling, call Inp.update before reading

	const A = {
		// y-axes are positive down.
		lStickX: 0, lStickY: 1,
		rStickX: 2, rStickY: 3
	}
	const B = {
		// right (face) buttons
		rDown: 0, rRight: 1, rLeft: 2, rUp: 3,  // positional
		A: 0, B: 1, X: 2, Y: 3,  // xbox
		cross: 0, circle: 1, square: 2, triangle: 3,  // playstation
		// front buttons
		lBumper: 4, rBumper: 5,    // upper buttons
		lTrigger: 6, rTrigger: 7,  // lower "buttons" (analog)
		// center buttons
		back: 8, start: 9, guide: 16,
		// stick press
		lStick: 10, rStick: 11,
		// left buttons (dpad)
		lUp: 12, lDown: 13, lLeft: 14, lRight: 15,
		dpadUp: 12, dpadDown: 13, dpadLeft: 14, dpadRight: 15, 
	}
	const gpNames = {}
	for(const name in B) if(Object.hasOwn(B,name)) {
		gpNames['b'+B[name]] ??= []
		gpNames['b'+B[name]].push(name)
	}
	for(const name in A) if(Object.hasOwn(A,name)) {
		gpNames['axis'+A[name]] ??= []
		gpNames['axis'+A[name]].push(name)
	}

	const isPrefixOf = (pre, str) => str.substr(0,pre.length) === pre
	const clearDevice = (dev, state) => {
		for(const name in state) if(Object.hasOwn(state, name)) {
			if(isPrefixOf(dev, name)) delete(state[name])
		}
	}
	const _update = (pre, key, value) => {
		if(state[pre+key] !== value) {
			Inp.changed(pre+key, value, value-(state[pre+key]??0))
			state[pre+key] = value  // save under numbered button/axis
			// also save under nicknames
			if(gpNames[key]) for(const name of gpNames[key]) {
				state[pre+name] = value
			}
		}
		// Set these unconditionally because we clear them every frame
		state['gp.'+key] = merge(value, state['gp.'+key]??0)
		if(gpNames[key]) for(const name of gpNames[key]) {
			state['gp.'+name] = merge(value, state['gp.'+name]??0)
		}
	}
	const connectedGamepads = new Set()
	Inp.update = () => {
		clearDevice('gp.', state)
		const gamepads = navigator.getGamepads()
		for(const gp of gamepads) {
			if(gp == null || !gp.connected) continue
			connectedGamepads.add(gp)
			const pre = 'gp'+(gp.index+1)+'.'
			for(let b=0; b<gp.buttons.length; ++b) {
				const button = gp.buttons[b]
				_update(pre, 'b'+b, button.value || +button.pressed)
			}
			for(let a=0; a<gp.axes.length; ++a) {
				_update(pre, 'axis'+a, gp.axes[a])
			}
		}
		for(const gp of connectedGamepads) {
			if(!gp.connected) clearDevice('gp'+gp.index+'.', state)
		}
	}

	return Inp
}
