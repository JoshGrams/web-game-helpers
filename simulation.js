// SPDX-FileCopyrightText: 2024 Joshua I. Grams <himself@joshgrams.com>
//
// SPDX-License-Identifier: MIT

// Returns a frameHandler function that you can pass to
// requestAnimationFrame.
//
// Pass in two functions:
//   * update(dt)
//   * draw(unsimulatedFrames, unsimulatedTime)
// It's perfectly fine to ignore these arguments if you don't need them.
// dt is the timestep, while the arguments to draw allow interpolating
// or extrapolating when running on a display that refreshes faster (or
// slower) than your simulation rate.
//
// Defaults to calling the update function 60 times a second.
// Set frameHandler.rate to change the fixed rate, or set it
// to 0 for a variable timestep.
//
// The frameHandler will also have an `fps` property that gives the
// average refresh rate that the game is successfully redrawing at,
// and a redrawRate property that estimates the hardware display
// refresh rate.
//
// To halt the simulation, set frameHandler.stop to true.
//
// I also have a timeScale property to run time slower (turtle
// mode?) or faster, i.e 0.5 timeScale would still ask update()
// to simulate 1/60 second but call it only 30 times per second.
const Simulation = (update, draw) => {
	// Average out the framerate - circular buffer
	const frames = { N: 30, i: -1, t: [1/60] }
	const addTime = (T,sec) => T.t[T.i = (T.i+1)%T.N] = sec
	const avgFPS = (T) => T.t.length/T.t.reduce((a,s)=>a+s, 0)
	const redrawRate = (T) => 1/T.t.reduce((m,s)=>Math.min(m,s),Infinity)

	// Record frame time, update and draw the simulation
	let lastFrame, unsimulated = 0
	const sim = (now) => {
		if(sim.stop) return
		now = now / 1000  // seconds
		// Condition and record the frame time
		if(lastFrame) {
			// Sliding average to show the player
			sim.fps = avgFPS(frames)
			// Fastest rate, rounded to the nearest quarter FPS
			// (the hardware display refresh rate?)
			sim.redrawRate = Math.round(redrawRate(frames)*4)/4
			// Avoid large timesteps when we refocus a hidden browser tab
			const dtRaw = Math.max(1/1000, Math.min(now - lastFrame, 2/sim.redrawRate))
			addTime(frames, dtRaw)
			// Round dt to an integer multiple of the refresh rate, to
			// avoid jitter in when we were called
			const dt = Math.round(dtRaw * sim.redrawRate) / sim.redrawRate
			unsimulated += dt * sim.timeScale
		}
		lastFrame = now
		// Simulate
		const step = sim.rate ? 1/sim.rate : unsimulated
		while(unsimulated > 0 && unsimulated >= step) {
			unsimulated = Math.max(0, unsimulated - step)
			update(step)
		}
		draw(unsimulated/step, unsimulated)
		requestAnimationFrame(sim)
	}
	sim.redrawRate = 60 // estimated hardware screen refresh rate
	sim.fps = 60  // average rate that the game is managing to run
	sim.rate = 60  // simulation steps per second: 0 or false for variable timestep
	sim.timeScale = 1  // run time slower (turtle mode?) or faster
	sim.stop = false  // Set true to stop the simulation

	return sim
}
