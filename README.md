# Web Game Helpers

Handy pieces for making little web games in vanilla JS. Mostly
just for me. Aiming for tools rather than frameworks, small pieces
that each do one thing with a minimum of fuss and (hopefully) a
minimum of restrictions.

## Simulation

A basic main game-loop. Give it an update and a draw function.
Runs the update with a fixed timestep, decoupled from the refresh
rate of the player's monitor.

You can set the fixed timestep to whatever, or run it with a
variable timestep. It passes the timestep to the update function
so you can compute things in per-second rather than per-frame
speeds if you prefer (which I do).

And it passes parameters to the draw function so you can
optionally interpolate or extrapolate positions for smoother
display when the simulation rate doesn't match the redraw rate.

So you can simply use it like `requestAnimationFrame` and ignore
all the callback parameters, but without your games running 2+
times as fast on high refresh-rate monitors. Or you can get
fancier with how you handle the edge cases.


## Input

Call it to install a keyboard listener that tracks which keys are
down. Clears the state when the window loses focus to avoid the
common bug where keys get "stuck" down when you switch away from
the window.

Call `input.update` in your game's update loop to poll any
gamepads that might be present.

Then `input.get` lets you use an input configuration object like
the following to let you name your inputs for what they do, and
get the states of all inputs at once (this gives an object with a
single value from 0 to 1 (or -1 to 1) for each input name:

```
let controls = input.get({
	jump: 'kb.Space',
	fire: ['kb.X', 'gp.rTrigger/20'],
	x: ['kb.D', '-kb.A', 'gp.lStickX'],
	y ['kb.S', '-kb.W', 'gp.lStickY'],
})
```

Note that to negate an input (to flip an axis or combine two
buttons into an axis), you can put a `-` before its name. To use
an axis as a button, add a `/` and a percent threshold (0-100).

If you don't care about gamepad inputs, you can drop the `kb.`
prefix and call input.getKeys (does not support negation or
thresholds):

```
let controls = input.getKeys({
	jump: 'Space',
	right: ['D', 'ArrowRight'],  left: ['A', 'ArrowLeft'],
	down ['S', 'ArrowDown'],  left: ['W', 'ArrowUp'],
})
```

The keyboard defaults to using scancodes so the (positionally
arranged) key bindings for your game won't get scattered across
the keyboard for people using non-QWERTY layouts. (Well, unless
they have an actual hardware layout, but there's nothing we can do
about that. At least you'll handle the majority of
French/Belgian/German users with AZERTY or QWERTZ keyboards
without having to think about it much.

If you really want the keys based on the character they send
instead of by their (QWERTY) position, you can use a `ch.` prefix
instead (character), or call input.getChar (I didn't write a
multi-get for this one, sorry. You could add it if you want).

It doesn't look like you can translate *back* from scancodes to
key legends to display in the UI (well, there's an experimental
API that works on secure pages in some browsers?). That's
annoying. But we *could* keep track of the association if the user
presses the key (e.g. to *set* a binding), since then we get both.
I'll have to think about that.

---

I also threw in a couple utility functions: `merge(a, b, ...)`
combines multiple button or axis values in a helpful way, adding
the largest positive and largest negative values. So if one stick
is held left and the other held right, they'll cancel each other
out. If one button out of a group is held down, it'll count as
being held.

And `dead(x, y, fraction) -> [x', y']` will apply a center
dead-zone to two axes, like a gamepad thumbstick: any input that's
less that `fraction` from the center will become 0, and it scales
the full deflection so it's still 1. It incidentally also clamps
the length so that the x,y pair is never longer than length 1.
It's often handy to be able to assume that in the math: for
instance you wouldn't be able to go faster by travelling on the
diagonal if your gamepad or joystick lets you push the stick to a
full 1,1 value in the corner of the square.

I find that a `fraction` of 0.1 is sufficient for most gamepads
until they get *extremely* janky and old, without being too
annoyingly large of a blank spot in the middle of the stick. You
can bump that up to 0.15 or even 0.2 if you want to be nice to
people who keep their gamepads until they really die. For most
games a bigger dead zone doesn't matter too much: you're just
going to push the stick all the way to the limit anyway...

I also added a smoothing function for joysticks, but I'm seeing
some weirdnesses when I use it so that probably needs further
debugging. I'm pretty sure I've used this math before, but...
hrmph. Anyway. You pass in the previous (smoothed) values, and the
raw ones from the joystick, plus the simulation timestep and the
time amount you want to smooth it over (0.2 seconds is a nice
amount to make it feel silky smooth with no jitter but not *too*
much delay), and get an `[x, y]` array back:

`smooth(xSmoothed, ySmoothed, xRaw, yRaw, dt, smoothTime) -> [x, y]`
