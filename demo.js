// SPDX-FileCopyrightText: 2024 Joshua I. Grams <himself@joshgrams.com>
//
// SPDX-License-Identifier: MIT

const canvas = document.getElementById('game')
const ctx = canvas.getContext('2d')
const Inp = Input()

const TURN = 2*Math.PI

let w = 640, h = 480  // haha. Updated to real values every frame.

const lerp = (t, a, b) => a + t*(b-a)

const Circle = (x, y, r, color) => ({
	x: x,  y: y,  r: r,  fill: color,
	vx: 0,  vy: 0,
	tail: []
})

drawCircle = (c,dt) => {
	const x = c.x + dt*c.vx
	const y = c.y + dt*c.vy
	ctx.fillStyle = c.fill
	for(i=c.tail.length-1; i>=0; --i) {
		const r = c.r * lerp(i/c.tail.length, 0.25, 1)
		ctx.beginPath()
		ctx.arc(c.tail[i][0], c.tail[i][1], r, 0,TURN)
		ctx.fill()
	}
	ctx.beginPath()
	ctx.arc(x, y, c.r, 0,TURN)
	ctx.fill()
}

updateCircle = (c,dt) => {
	if(c.vx == 0 && c.vy == 0) {
		c.tail.shift()
	} else {
		c.tail.push([c.x, c.y])
		if(c.tail.length > 30) c.tail.shift()
	}
	if(c.controls) {
		const input = Inp.get(c.controls)
		const [mx, my] = Inp.dead(input.x, input.y, 0.1)
		const accel = 500  // pixels per second per second
		c.vx += dt*accel*mx
		c.vy += dt*accel*my
		if(input.stop) { c.vx = c.vy = 0 }
	}
	const x = Math.max(c.r, Math.min(c.x, w - c.r))
	const y = Math.max(c.r, Math.min(c.y, h - c.r))
	// we hit the wall, bounce off
	if(x < c.x) c.vx = -Math.abs(c.vx)
	else if(x > c.x) c.vx = Math.abs(c.vx), y
	if(y < c.y) c.vy = -Math.abs(c.vy)
	else if(y > c.y) c.vy = Math.abs(c.vy)
	// add new velocity
	c.x = x + dt*c.vx
	c.y = y + dt*c.vy
}

const player = Circle(100, 100, 25, '#300')
player.vx = 25;  player.vy = 40
player.controls = {
	x: ['kb.D', '-kb.A', 'kb.ArrowRight', '-kb.ArrowLeft', 'gp.lStickX'],
	y: ['kb.S', '-kb.W', 'kb.ArrowDown', '-kb.ArrowUp', 'gp.lStickY'],
	stop: ['kb.Space', 'gp.lTrigger/20'],
}
const circles = [
	Circle(200, 100, 30, '#bb6'),
	Circle(600, 500, 80, '#bb6'),
	Circle(300, 600, 40, '#bb6'),
	Circle(100, 400, 50, '#bb6'),
	player
]

const update = (dt) => {
	Inp.update()
	circles.forEach(c=>updateCircle(c, dt))
}
const draw = (unsimulatedFrames, unsimulatedTime) => {
	w = canvas.clientWidth;  h = canvas.clientHeight
	canvas.width = w;  canvas.height = h
	ctx.fillStyle = '#cc7'
	ctx.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight)
	circles.forEach(c=>drawCircle(c,unsimulatedTime))
}

const simulation = Simulation(update, draw)
requestAnimationFrame(simulation)
